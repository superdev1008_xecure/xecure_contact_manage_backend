<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('encryption');
		$this->load->model('register_model');
	}

	function index()
	{
		$this->load->view('register_view');
	}

	function register_validation()
	{
		$encrypted_password = $this->encryption->encrypt($this->input->post('password'));
		$data_post = array(
			'company_name' => $this->input->post('company_name'),
			'street_address' => $this->input->post('street_address'),
			'city' => $this->input->post('city'),
			'state' => $this->input->post('state'),
			'code' => $this->input->post('code'),
			'country' => $this->input->post('country'),
			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
			'email' => $this->input->post('email'),
			'phone_number' => $this->input->post('phone_number'),
			'password' => $encrypted_password
		);

		if ($this->register_model->insertCompany($data_post))
		{
			//send confirm mail
			/*if ($this->register_model->sendEmail($data_post['email'])) {
				$this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Successfully registered. Please confirm the mail that has been sent to your email. </div>');
				$session_data = array(
					'company' => $data_post['company_name']
				);
				$this->session->set_userdata($session_data);
				redirect(base_url() . 'Home');
			}*/
			$this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Successfully registered. Please confirm the mail that has been sent to your email. </div>');
			$session_data = array(
				'company' => $data_post['company_name']
			);
			$this->session->set_userdata($session_data);
			redirect(base_url() . 'Home');
		}
		else{
			$this->session->set_flashdata('error', "<strong>Failed!</strong> The company name is already used by another.");
			redirect(register);
		}
	}
}
