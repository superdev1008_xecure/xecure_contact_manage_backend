<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('encryption');
		$this->load->model('login_model');
	}

	function index()
	{
		if ($this->session->userdata('company') != '')
			redirect('home');
		else
			$this->load->view('login_view');
	}

	function login_validation(){
		$company_name = $this->input->post('company_name');
		$password = $this->input->post('password');
		//model function
		$this->load->model('Login_model');
		if ($this->Login_model->can_login($company_name, $password))
		{
			$session_data = array(
				'company' => $company_name
			);
			$this->session->set_userdata($session_data);
			redirect('home');
		}
		else {
			$this->session->set_flashdata('error', "Invalid company name or password!");
			redirect('login');
		}
	}
}
