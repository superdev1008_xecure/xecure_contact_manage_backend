<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	
	function __construct() {
        parent::__construct();
        $this->load->model('csv_model');
        $this->load->library('Csvimport');
        $this->load->helper('download');
    }

	public function index()
	{
        if ($this->session->userdata("company")) {
			$data['company_name'] = $this->session->userdata('company');
            $data['addressbook'] = $this->csv_model->get_addressbook($data['company_name']);            
            $data['page_content'] = 'import_content_view';
            $this->load->view('home_view', $data);
        }else{
            redirect("login");
        }
	}


    function import(){
        $this->session->set_flashdata('current_content', "import_content_view");
        redirect("home");
    }

    function help(){
        $this->session->set_flashdata('current_content', "help_content_view");
        redirect("home");
    }

    function logout(){
        $this->session->sess_destroy();
        redirect("login");
    }

    function download($fileName = NULL) {
       if ($fileName) {
            $file = realpath ( "download" ) . "/" . $fileName;
            // check file exists    
            if (file_exists ( $file )) {
            // get file content
            $data = file_get_contents ( $file );
            //force download
            force_download ( $fileName, $data );
            } else {
                // Redirect to base url
                redirect ('home/help');
            }
        }
    }

	function importcsv() {
        $data['addressbook'] = $this->csv_model->get_addressbook($this->session->userdata('company'));
        $data['error'] = '';    //initialize image upload error array to empty
 
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'csv';
        $config['max_size'] = '1000';
 
        $this->load->library('upload', $config);
 
 
        // If upload failed, display error
        if (!$this->upload->do_upload()) {
            $this->session->set_flashdata('error', $this->upload->display_errors());
            redirect("home");
            // $this->load->view('home_view', $data);
        } else {
            $file_data = $this->upload->data();
            $file_path =  './uploads/'.$file_data['file_name'];
 
            if ($this->csvimport->get_array($file_path)) {
                $csv_array = $this->csvimport->get_array($file_path);
                $company_name = $this->session->userdata('company');
                foreach ($csv_array as $row) {
                    $keys = array_keys($row);
                    $detect_array = array(
                        'DEPARTMENT NAME',
                        'SUB-DEPARTMENT NAME',
                        'SIP-ACCOUNT',
                        'FIRST NAME',
                        'LAST NAME',
                        'EMAIL ADDRESS',
                        'OFFICE PHONE NUMBER',
                        'MOBILE PHONE NUMBER'
                    );
                    $result_cmp = implode(',', array_diff($keys, $detect_array));
                    if ($result_cmp != "") {
                        $this->session->set_flashdata('error', "The fields of contacts you edited to upload is not allowed.");
                        redirect("home");
                    }
                    $insert_data = array(
                        'company_name'=>$company_name,
                        'department_name'=>$row['DEPARTMENT NAME'],
                        'sub_department_name'=>$row['SUB-DEPARTMENT NAME'],
                        'sip_account'=>$row['SIP-ACCOUNT'],
                        'first_name'=>$row['FIRST NAME'],
                        'last_name'=>$row['LAST NAME'],
                        'email_address'=>$row['EMAIL ADDRESS'],
                        'office_phone_number'=>$row['OFFICE PHONE NUMBER'],
                        'mobile_phone_number'=>$row['MOBILE PHONE NUMBER']
                    );
                    $this->csv_model->insert_csv($insert_data);
                }
                $this->session->set_flashdata('success', 'Contacts Imported Succesfully');
                redirect(base_url().'home');
                //echo "<pre>"; print_r($insert_data);
            } else {
                $this->session->set_flashdata('error', "Error occured.");
                redirect("home");
            }
        }
 
    }    
}