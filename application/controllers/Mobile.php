<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mobile extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('csv_model');
		$this->load->model('login_model');
	}
	
	function getContactsByCompany(){
		$contactInfo['company_name'] = $this->correctParam("company_name", "");
		$result = $this->csv_model->findContacts($contactInfo);
		
		echo json_encode($result);
	}
	
	function getContactsByCompanyAndDepartment(){
		$contactInfo['company_name'] = $this->correctParam("company_name", "");
		$contactInfo['department_name'] = $this->correctParam("department_name", "");
		$result = $this->csv_model->findContacts($contactInfo);
		
		echo json_encode($result);
	}
	
	function getContactsByAll(){
		$contactInfo['company_name'] = $this->correctParam("company_name", "");
		$contactInfo['department_name'] = $this->correctParam("department_name", "");
		$contactInfo['sub_department_name'] = $this->correctParam("sub_department_name", "");
		$result = $this->csv_model->findContacts($contactInfo);
		
		echo json_encode($result);
	}
	
	function getCompanies(){
		$result = $this->login_model->findCompanies();
		
		echo json_encode($result);
	}
	
	function getDepartments(){
		$company_name = $this->correctParam("company_name", "");
		$result = $this->csv_model->findDepartments($company_name);
		
		echo json_encode($result);
	}
	
	function getSubDepartments(){
		$company_name = $this->correctParam("company_name", "");
		$department_name = $this->correctParam("department_name", "");
		$info = array(
			'company_name' => $company_name,
			'department_name' => $department_name
		);
		$result = $this->csv_model->findSubDepartments($info);
		
		echo json_encode($result);
	}
	
	function addContact(){
		$insert_data = array(
       	    'company_name'=>$this->correctParam("company_name", ""),
       	    'department_name'=>$this->correctParam("department_name", ""),
       	    'sub_department_name'=>$this->correctParam("sub_department_name", ""),
       	    'sip_account'=>$this->correctParam("sip_account", ""),
       	    'first_name'=>$this->correctParam("first_name", ""),
       	    'last_name'=>$this->correctParam("last_name", ""),
       	    'email_address'=>$this->correctParam("email_address", ""),
       	    'office_phone_number'=>$this->correctParam("office_phone_number", ""),
       	    'mobile_phone_number'=>$this->correctParam("mobile_phone_number", "")
       	);
        $this->csv_model->insert_csv($insert_data);
	}
}
