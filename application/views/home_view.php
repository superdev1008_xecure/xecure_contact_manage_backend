<!DOCTYPE HTML>
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Xecure System</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="" />

  <!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/icons/icon.png">

	<link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css">
	<!-- Flexslider  -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/flexslider.css">
	<!-- Flaticons  -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/fonts/flaticon/font/flaticon.css">
	<!-- Owl Carousel -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/owl.carousel.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/owl.theme.default.min.css">
	<!-- Theme style  -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">

	<!-- Modernizr JS -->
	<script src="<?php echo base_url();?>assets/js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>
	<div id="colorlib-page">
		<a href="#" class="js-colorlib-nav-toggle colorlib-nav-toggle"><i></i></a>
		<aside id="colorlib-aside" role="complementary" class="border js-fullheight">
			<h1 id="colorlib-logo"><a href="<?php base_url(); ?>home/import">Company Name : <strong>
                        <?php echo $company_name; ?>
                    </strong></a></h1>
			<nav id="colorlib-main-menu" role="navigation">
				<ul>
					<li <?php if ($this->session->flashdata("current_content") == "import_content_view") { ?>
						class="colorlib-active"
					<?php } elseif ($this->session->flashdata("current_content") != "help_content_view") {?>
						class="colorlib-active"
					<?php }?>><a href="<?php base_url(); ?>home/import">Import Contacts</a></li>
					<li <?php if ($this->session->flashdata("current_content") == "help_content_view") { ?>
						class="colorlib-active"
					<?php }?>><a href="<?php base_url(); ?>home/help">Help</a></li>
				</ul>
			</nav>

			<div class="colorlib-footer">
				<a href="<?php echo base_url();?>home/logout">Log Out</a>
				<br><br>
				<a href="#" target="_blank"><p>&copy;XECURE Address Book</p></a>
			</div>

		</aside>

		<div id="colorlib-main">
			<?php 
				if ($this->session->flashdata("current_content")) {
					$this->load->view($this->session->flashdata("current_content"));
				}
				else{
					$this->load->view($page_content);	
				}?> 
		</div>
	</div>

	<!-- jQuery -->
	<script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="<?php echo base_url();?>assets/js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="<?php echo base_url();?>assets/js/jquery.waypoints.min.js"></script>
	<!-- Flexslider -->
	<script src="<?php echo base_url();?>assets/js/jquery.flexslider-min.js"></script>
	<!-- Sticky Kit -->
	<script src="<?php echo base_url();?>assets/js/sticky-kit.min.js"></script>
	<!-- Owl carousel -->
	<script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
	<!-- Counters -->
	<script src="<?php echo base_url();?>assets/js/jquery.countTo.js"></script>
	
	
	<!-- MAIN JS -->
	<script src="<?php echo base_url();?>assets/js/home.js"></script>

	<script src="<?php echo base_url();?>assets/daterangepicker/moment.min.js"></script>
    <script src="<?php echo base_url();?>assets/daterangepicker/daterangepicker.js"></script>

    <script src="<?php echo base_url();?>assets/countdowntime/countdowntime.js"></script>

	<script src="<?php echo base_url();?>assets/js/readCSV.js"></script>    

	</body>
</html>

