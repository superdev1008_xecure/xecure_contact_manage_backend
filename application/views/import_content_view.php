<div class="container" style="margin-top:50px">    
     <br>
    <?php if ($this->session->flashdata('error')): ?>
        <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?> Click <a href="<?php base_url(); ?>home/help">here</a> to help.</div>
    <?php endif; ?>
    <?php if ($this->session->flashdata('success') == TRUE): ?>
        <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
    <?php endif; ?>
    <h2>XECURE Addressbook Import</h2>
    <form method="post" action="<?php echo base_url() ?>home/importcsv" enctype="multipart/form-data">
        <button class="btn btn-primary" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Add Contact List</button>
        <div class="image-upload-wrap">
          <input class="file-upload-input" type='file' onchange="readURL(this);" accept=".csv" name="userfile"/>
          <div class="drag-text">
            <h3>Drag and drop a file or select add Contacts</h3>
          </div>
        </div>
        <div class="file-upload-content">
          <img class="file-upload-image" src="<?php echo base_url();?>assets/images/uploaded_file.png" alt="conact list" />
          <div class="image-title-wrap">
            <button type="button" onclick="removeUpload()" class="btn btn-danger">Remove <span class="image-title">Uploaded Image</span></button>
          </div>
        </div>
        <br>
        <div class="text-center">
            <input type="submit" name="submit" value="UPLOAD" class="btn btn-success">
        </div>
    </form>
    <br><br>
    <table class="table table-striped table-hover table-bordered">
        <caption>Address Book List</caption>
        <thead>
            <tr>
                <th>DEPARTMENT NAME</th>
                <th>SUB-DEPARTMENT NAME</th>
                <th>SIP-ACCOUNT</th>
                <th>FIRST NAME</th>
                <th>LAST NAME</th>
                <th>EMAIL ADDRESS</th>
                <th>OFFICE PHONE NUMBER</th>
                <th>MOBILE PHONE NUMBER</th>
            </tr>
        </thead>
        <tbody>
            <?php if ($addressbook == FALSE): ?>
                <tr><td colspan="8">There are currently No Addresses</td></tr>
            <?php else: ?>
                <?php foreach ($addressbook as $row): ?>
                    <tr>
                        <td><?php echo $row['department_name']; ?></td>
                        <td><?php echo $row['sub_department_name']; ?></td>
                        <td><?php echo $row['sip_account']; ?></td>
                        <td><?php echo $row['first_name']; ?></td>
                        <td><?php echo $row['last_name']; ?></td>
                        <td><?php echo $row['email_address']; ?></td>
                        <td><?php echo $row['office_phone_number']; ?></td>
                        <td><?php echo $row['mobile_phone_number']; ?></td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
        </tbody>
    </table>
    <hr>
</div>