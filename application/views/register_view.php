<!DOCTYPE html>
<html lang="en">
<head>
	<title>Register Xecure System</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="<?php echo base_url();?>assets/image/png" href="<?php echo base_url();?>assets/images/icons/icon.png"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/user.css">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form id="register_form" class="register100-form validate-form" method="post" action="<?php echo base_url();?>register/register_validation">
					<?php if ($this->session->flashdata('error')) { ?>
						<div class="alert alert-danger">
							<h5>
								<?php echo $this->session->flashdata('error');?>
							</h5>
						</div>
					<?php } ?>
					<span class="login100-form-title p-b-34">
						SEO Sign UP
					</span>
					<!-- company name -->
					<div class="wrap-input100 rs1-wrap-input100 validate-input m-b-20" data-validate="Type company name">
						<input class="input100" type="text" name="company_name" placeholder="Company name">
						<span class="focus-input100"></span>
					</div>
					<!-- street address -->
					<div class="wrap-input100 rs1-wrap-input100 validate-input m-b-20" data-validate="Type street address">
						<input class="input100" type="text" name="street_address" placeholder="Street address">
						<span class="focus-input100"></span>
					</div>
					<!-- city -->
					<div class="wrap-input100 rs1-wrap-input100 validate-input m-b-20" data-validate="Type city" style="width: 50%">
						<input class="input100" type="text" name="city" placeholder="City">
						<span class="focus-input100"></span>
					</div>
					<!-- State -->
					<div class="wrap-input100 rs1-wrap-input100 validate-input m-b-20" data-validate="Type state" style="width: 50%">
						<input class="input100" type="text" name="state" placeholder="State">
						<span class="focus-input100"></span>
					</div>
					<!-- postal/zip code -->
					<div class="wrap-input100 rs1-wrap-input100 validate-input m-b-20" data-validate="Type Postal or Zip Code" style="width: 50%">
						<input class="input100" type="text" name="code" placeholder="Postal / Zip Code">
						<span class="focus-input100"></span>
					</div>
					<!-- country -->
					<div class="wrap-input100 rs1-wrap-input100 validate-input m-b-20" data-validate="Select Country" style="width: 50%">
						<input class="input100" type="text" name="country" placeholder="Country">
						<span class="focus-input100"></span>
					</div>
					<!-- first name -->
					<div class="wrap-input100 rs1-wrap-input100 validate-input m-b-20" data-validate="Type first name" style="width: 50%">
						<input class="input100" type="text" name="first_name" placeholder="First name">
						<span class="focus-input100"></span>
					</div>
					<!-- last name -->
					<div class="wrap-input100 rs1-wrap-input100 validate-input m-b-20" data-validate="Type last name" " style="width: 50%">
						<input class="input100" type="text" name="last_name" placeholder="Last name">
						<span class="focus-input100"></span>
					</div>
					<!-- email -->
					<div class="wrap-input100 rs1-wrap-input100 validate-input m-b-20" data-validate="Type contact email">
						<input class="input100" type="email" name="email" placeholder="Contact Email">
						<span class="focus-input100"></span>
					</div>
					<!-- phone -->
					<div class="wrap-input100 rs1-wrap-input100 validate-input m-b-20" data-validate="Type contact number">
						<input class="input100" type="phone_number" name="phone_number" placeholder="Contact Number">
						<span class="focus-input100"></span>
					</div>
					<!-- password -->
					<div class="wrap-input100 rs2-wrap-input100 validate-input m-b-20" data-validate="Type password">
						<input id="password" class="input100" type="password" name="password" placeholder="Password">
						<span class="focus-input100"></span>
					</div>
					<!-- confirm password -->
					<div class="wrap-input100 rs2-wrap-input100 validate-input m-b-20" data-validate="Type password to confirm" data-match="not match">
						<input id="confirm_password" class="input100" type="password" name="password" placeholder="Confirm Password">
						<span class="focus-input100"></span>
					</div>

					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Register Company
						</button>
					</div>

					<div class="w-full text-center p-t-24 p-b-10">
						<a href="<?php echo base_url()?>" class="txt3">
							Log In
						</a>
					</div>
				</form>

				<div class="login100-more" style="background-image: url('<?php echo base_url();?>assets/images/bg-01.jpg');"></div>
			</div>
		</div>
	</div>
	
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/bootstrap/js/popper.js"></script>
	<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/select2/select2.min.js"></script>
	<script>
		$(".selection-2").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect1')
		});
	</script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/daterangepicker/moment.min.js"></script>
	<script src="<?php echo base_url();?>assets/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>assets/js/main.js"></script>

</body>
</html>