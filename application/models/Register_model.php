<?php
class Register_model extends CI_Model
{
	function insertCompany($data)
	{
		$this->db->where('company_name', $data['company_name']);
		$query_user = $this->db->get('users');
		if ($query_user->num_rows() > 0) {
			return false;
		}

		$this->db->insert('users', $data);

		return true;
	}

	//send confirm mail
    public function sendEmail($receiver){
        $subject = 'Verify email address';  //email subject
        
        // sending confirmEmail($receiver) function calling link to the user, inside message body
        $message = 'Dear User,<br><br> Please click on the below activation link to verify your email address<br><br>
        <a href=\'http://www.localhost/codeigniter/Signup_Controller/confirmEmail/'.md5($receiver).'\'>http://www.localhost/codeigniter/Signup_Controller/confirmEmail/'. md5($receiver) .'</a><br><br>Thanks';
        //config email settings
        $config = Array(
			'protocol' => 'smtp',
			'smtp_host' => 'dedrelay.secureserver.net',
			'mailtype'  => 'html', 
			'charset'   => 'iso-8859-1',
			'wordwrap'   => TRUE
		);
        $this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from('hello@xecure.systems');
        $this->email->to($receiver);
        $this->email->subject($subject);
        $this->email->message($message);
        // Set to, from, message, etc.
        if($this->email->send()){
			//for testing
            echo "sent to: ".$receiver."<br>";
			echo "from: ".$from. "<br>";
			echo "protocol: ". $config['protocol']."<br>";
			echo "message: ".$message;
            return true;
        }else{
            echo "email send failed";
            return false;
        }
    }
}
?>