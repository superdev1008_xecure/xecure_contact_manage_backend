<?php
 
class Csv_model extends CI_Model {
 
    function __construct() {
        parent::__construct();
 
    }
 
    function get_addressbook($company_name) {
		$this->db->where('company_name', $company_name);
        $query = $this->db->get('addressbook');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return FALSE;
        }
    }
 
    function insert_csv($data) {
        $this->db->where('mobile_phone_number', $data['mobile_phone_number']);
        $query = $this->db->get('addressbook');
        if ($query->num_rows() > 0) {
            $this->db->update('addressbook', $data);    
        }else{
            $this->db->insert('addressbook', $data);
        }
    }
	
	function findContacts($contactInfo){
		$query = $this->db->get_where("addressbook", $contactInfo);
		$result["success"] = $query->num_rows() == 0 ? false : true;
		$result["data"] = $query->result_array();

		return $result;
	}

	function findDepartments($company_name){
		$this->db->where('company_name', $company_name);
        $query = $this->db->get('addressbook');
		$result = array();
		foreach ($query->result() as $row){
			$department = $row->department_name;
			if(!empty($department) && !in_array($department, $result)){
				array_push($result, $department);
			}
		}
		
		return $result;
	}
	
	function findSubDepartments($info){
		$this->db->where($info);
        $query = $this->db->get('addressbook');
		$result = array();
		foreach ($query->result() as $row){
			$sub_department = $row->sub_department_name;
			if(!empty($sub_department) && !in_array($sub_department, $result)){
				array_push($result, $sub_department);
			}
		}
		
		return $result;
	}
}