<?php
class login_model extends CI_Model
{
	function can_login($companyname, $password)
	{
		$this->db->where('company_name', $companyname);
		$query = $this->db->get('users');
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$store_password = $this->encryption->decrypt($row->password);
				if ($password == $store_password){
					return true;
				}	
			}
		}
		return false;
	}
	
	function findCompanies(){
		$query = $this->db->get('users');
		$result = array();
		foreach ($query->result() as $row){
			$company = $row->company_name;
			if(!in_array($company, $result)){
				array_push($result, $company);
			}
		}
		
		return $result;
	}
}
?>